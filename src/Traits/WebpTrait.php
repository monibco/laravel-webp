<?php

namespace Monibco\LaravelWebp\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;

trait WebpTrait
{
    /**
     * @var UploadedFile
     */
    protected $image;

    /**
     * @var int
     */
    protected $quality;

    /**
     * @param UploadedFile $image
     * @return WebpTrait
     */
    public function make($image): self
    {
        $this->quality = Config::get('laravel-webp.default_quality');
        $this->image = $image;

        return $this;
    }

    /**
     * @param $quality
     * @return WebpTrait
     */
    public function quality($quality): self
    {
        $this->quality = $quality;

        return $this;
    }
}